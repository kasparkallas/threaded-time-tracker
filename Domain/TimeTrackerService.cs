﻿namespace Domain
{
    public class TimeTrackerService
    {
        private readonly IRepository _repository;

        public TimeTrackerService(IRepository repository)
        {
            _repository = repository;
        }

        public TimeThread StartThread(User user, string description)
        {
            var thread = TimeThread.Start(user);
            thread.Description = description;

            _repository.Add(thread);

            return thread;
        }

        public TimeThread StartThread(User user, TimeThread parentThread, string description)
        {
            var thread = parentThread.StartChild(user);
            thread.Description = description;

            _repository.Add(thread);

            return thread;
        }

        public void StopThread(TimeThread thread)
        {
            thread.Stop();

            _repository.Update(thread);
        }
    }
}
