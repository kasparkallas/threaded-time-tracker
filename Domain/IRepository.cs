﻿using System;
using System.Linq;

namespace Domain
{
    public interface IRepository
    {
        void Add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        T Load<T>(Guid id) where T : class;
        IQueryable<T> Query<T>() where T : class;
    }
}
