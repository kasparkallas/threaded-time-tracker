﻿using System;
using System.Collections.Generic;

namespace Domain
{
    /// <summary>
    /// A grouping of users (e.g. employees of a company)
    /// </summary>
    public class Team
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<TeamMember> Members { get; set; }
    }
}
