﻿using System;

namespace Domain
{
    public class TeamMember
    {
        public Guid Id { get; set; }
        public virtual User User { get; set; }
        public virtual Team Team { get; set; }
        public DateTime ActiveFrom { get; set; }
        public DateTime? ActiveTo { get; set; }
    }
}