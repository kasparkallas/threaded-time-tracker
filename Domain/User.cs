﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class User
    {
        private User()
        {
        }

        public User(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }

        protected virtual ICollection<TimeThread> Threads { get; private set; }
        protected virtual ICollection<Project> VisibleProjects { get; private set; }
    }
}
