﻿using System;

namespace Domain
{
    /// <summary>
    /// A marker for point in time when something significant happened.
    /// </summary>
    public class TimePost
    {
        public Guid Id { get; private set; }
        public string Description { get; set; }
        public Guid UserId { get; private set; }
        public DateTimeOffset HappenedAt { get; private set; }

        public TimePost(User user, DateTimeOffset happenedAt)
        {
            Id = Guid.NewGuid();
            HappenedAt = happenedAt;
            UserId = user.Id;
        }
    }
}