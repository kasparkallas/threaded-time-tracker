﻿using System;
using System.Collections.Generic;

namespace Domain
{
    /// <summary>
    /// Examples: "My own time", "Company X" etc
    /// </summary>
    public class Project
    {
        public Guid Id { get; set; }

        public virtual ICollection<TimeThread> TrackedTimeUnderProject { get; set; }
    }
}
