﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Domain
{
    /// <summary>
    /// Main entity for tracking time. A 'tracking of time' can be nested inside another 'tracking of time'.
    /// </summary>
    public class TimeThread
    {
        public Guid Id { get; private set; }

        [MaxLength(200)]
        public string Description { get; set; }
        public bool IsBillable { get; set; }

        public Guid UserId { get; private set; }
        public virtual User User { get; private set; }
        public Guid? ParentId { get; private set; }

        public virtual ICollection<TimeThread> Children { get; private set; } = new List<TimeThread>();
        public int LevelsDeep { get; private set; }
        public DateTimeOffset StartedAt { get; private set; }
        public DateTimeOffset? StoppedAt { get; private set; }

        public virtual Project Project { get; set; }

        private TimeThread()
        {
        }

        internal static TimeThread Start(User user)
        {
            return new TimeThread(user);
        }

        private TimeThread(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            User = user;
            UserId = user.Id;
            Id = Guid.NewGuid();
            StartedAt = DateTimeOffset.Now;
        }

        internal TimeThread StartChild(User user)
        {
            if (user.Id != this.UserId) throw new ArgumentException(nameof(user));

            var child = new TimeThread(user, this);
            Children.Add(child);
            return child;
        }

        private TimeThread(User user, TimeThread parent) : this(user)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));

            ParentId = parent.Id;
            LevelsDeep = parent.LevelsDeep + 1;
        }

        internal void Stop()
        {
            if (Children.Any(x => x.StoppedAt == null))
            {
                throw new InvalidOperationException("All children threads must be stopped before stopping parent thread.");
            }

            if (StoppedAt != null)
            {
                throw new InvalidOperationException("Thread already stopped.");
            }

            StoppedAt = DateTimeOffset.Now;
        }
    }
}
