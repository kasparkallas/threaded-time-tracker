﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Services
{
    public class TimeThreadsHttpClient : HttpClient
    {
        public TimeThreadsHttpClient(string bearer)
        {
            BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("ApiRoot"));
            DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", bearer);
        }

        public async Task<IEnumerable<TimeThreadDto>> GetAll(Guid userId)
        {
            var response = await this.GetAsync($"api/threads/user/{userId}");
            ThrowIfNotSuccess(response);
            return await response.Content.ReadAsAsync<TimeThreadDto[]>();
        }

        public async Task Start(Guid userId, string description, Guid? parentThreadId = null)
        {
            var postContent = new
            {
                userId,
                description,
                parentThreadId
            };
            var response = await this.PostAsJsonAsync("api/threads/start", postContent);
            ThrowIfNotSuccess(response);
        }

        public async Task Stop(Guid threadId)
        {
            var response = await this.PostAsJsonAsync("api/threads/stop", threadId);
            ThrowIfNotSuccess(response);
        }

        private void ThrowIfNotSuccess(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Problems with the API... {response.ReasonPhrase}");
            }
        }
    }
}