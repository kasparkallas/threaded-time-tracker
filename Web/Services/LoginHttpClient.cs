﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Identity;
using Newtonsoft.Json;

namespace Web.Services
{
    public class LoginHttpClient : HttpClient
    {
        public LoginHttpClient()
        {
            BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("ApiRoot"));
        }

        public async Task<TokenResult> LogIn(Guid userId, string userName, string password)
        {
            var parameters = new Dictionary<string, string>
            {
                {"client_id", "web"},
                { "grant_type", "password"},
                { "username", userName},
                { "password", password}
            };

            var response = await this.PostAsync("token", new FormUrlEncodedContent(parameters));
            var tokenResult = await response.Content.ReadAsAsync<TokenResult>();

            using (var context = new ApplicationDbContext())
            {
                context.UserAccessTokens.Add(new UserAccessToken
                {
                    AccessToken = tokenResult.AccessToken,
                    UserId = userId,
                    ExpiresAt = tokenResult.ExpiresAt
                });
                context.SaveChanges();
            }

            this.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenResult.AccessToken);

            return tokenResult;
        }

        public class TokenResult
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }
            [JsonProperty(".expires")]
            public DateTime ExpiresAt { get; set; }
        }
    }
}