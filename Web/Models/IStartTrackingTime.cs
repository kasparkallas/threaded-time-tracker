﻿using System;

namespace Web.Models
{
    public interface IStartTrackingTime
    {
        string Description { get; }
        Guid? ParentThreadId { get; }
    }
}