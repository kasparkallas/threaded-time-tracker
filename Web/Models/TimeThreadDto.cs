﻿using System;
using System.Collections.Generic;

namespace Web.Models
{
    public class TimeThreadDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid? ParentId { get; set; }
        public string Description { get; set; }
        public bool IsBillable { get; set; }

        public ICollection<TimeThreadDto> Children { get; set; }
        public int LevelsDeep { get; set; }
        public DateTimeOffset StartedAt { get; set; }
        public DateTimeOffset? StoppedAt { get; set; }
    }
}