﻿using System;

namespace Web.Models
{
    public interface IStopTrackingTime
    {
        Guid LastTrackingThreadId { get; }
    }
}