﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Web.Models
{
    public class TimeTrackerViewModel : IStartTrackingTime, IStopTrackingTime
    {
        public bool IsTrackingTime { get; set; }
        public string Description { get; set; }
        public Guid? ParentThreadId => LastTrackingThreadId;
        public TimeThreadDto LastTrackingThread { get; set; }
        public Guid LastTrackingThreadId => LastTrackingThread?.Id ?? Guid.Empty;
        public IEnumerable<TimeThreadDto> StoppedThreads { get; set; } = Enumerable.Empty<TimeThreadDto>();
        public IEnumerable<TimeThreadDto> TrackingThreads { get; set; } = Enumerable.Empty<TimeThreadDto>();
    }
}