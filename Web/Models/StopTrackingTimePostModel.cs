﻿using System;

namespace Web.Models
{
    public class StopTrackingTimePostModel : IStopTrackingTime
    {
        public Guid LastTrackingThreadId { get; set; }
    }
}