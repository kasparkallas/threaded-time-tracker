﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class StartTrackingTimePostModel : IStartTrackingTime
    {
        [MaxLength(200)]
        public string Description { get; set; }
        public Guid? ParentThreadId { get; set; }
    }
}