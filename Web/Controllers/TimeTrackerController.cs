﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Identity;
using Microsoft.AspNet.Identity;
using Web.Models;
using Web.Services;

namespace Web.Controllers
{
    [Authorize]
    public class TimeTrackerController : Controller
    {
        private TimeThreadsHttpClient _timeThreadsHttpClient;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // TODO: Remove this hack...
            using(var context = new ApplicationDbContext())
            {
                var bearer = context.UserAccessTokens
                    .OrderByDescending(x => x.ExpiresAt)
                    .First(x => x.UserId == this.UserId)
                    .AccessToken;

                _timeThreadsHttpClient = new TimeThreadsHttpClient(bearer);
            }

            base.OnActionExecuting(filterContext);
        }

        public Guid UserId => new Guid(this.User.Identity.GetUserId());

        public async Task<ActionResult> Index()
        {
            var userThreads = (await _timeThreadsHttpClient.GetAll(UserId))
                .OrderByDescending(x => x.StartedAt)
                .ToList();

            var lastTrackingThread = userThreads
                .FirstOrDefault(x => x.StoppedAt == null);

            var model = new TimeTrackerViewModel
            {
                LastTrackingThread = lastTrackingThread,
                IsTrackingTime = lastTrackingThread != null,
                StoppedThreads = userThreads.Where(x => x.StoppedAt != null).OrderByDescending(x => x.StartedAt),
                TrackingThreads = userThreads.Where(x => x.StoppedAt == null).OrderByDescending(x => x.StartedAt)
            };

            return View(nameof(Index), model);
        }

        [HttpPost]
        public async Task<ActionResult> Start(StartTrackingTimePostModel model)
        {
            if (!ModelState.IsValid)
            {
                return await Index();
            }

            await _timeThreadsHttpClient.Start(
                userId: UserId,
                description: model.Description,
                parentThreadId: model.ParentThreadId);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<ActionResult> Stop(StopTrackingTimePostModel model)
        {
            if (!ModelState.IsValid)
            {
                return await Index();
            }

            await _timeThreadsHttpClient.Stop(threadId: model.LastTrackingThreadId);

            return RedirectToAction(nameof(Index));
        }
    }
}