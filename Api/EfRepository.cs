﻿using System;
using System.Data.Entity;
using System.Linq;
using Domain;

namespace Api
{
    public class EfRepository : IRepository
    {
        private readonly DomainDbContext _domainDbContext;

        public EfRepository(DomainDbContext domainDbContext)
        {
            _domainDbContext = domainDbContext;
        }

        public void Add<T>(T entity) where T : class
        {
            _domainDbContext.Set<T>().Add(entity);
            _domainDbContext.SaveChanges();
        }

        public void Update<T>(T entity) where T : class
        {
            var entityEntry = _domainDbContext.Entry(entity);
            entityEntry.State = EntityState.Modified;
            _domainDbContext.SaveChanges();
        }

        public T Load<T>(Guid id) where T : class
        {
            return _domainDbContext.Set<T>().Find(id);
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return _domainDbContext.Set<T>();
        }
    }
}