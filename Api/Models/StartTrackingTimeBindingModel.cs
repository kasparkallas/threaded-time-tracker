﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Models
{
    public class StartTrackingTimeBindingModel
    {
        public Guid UserId { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }

        public Guid? ParentThreadId { get; set; }
    }
}