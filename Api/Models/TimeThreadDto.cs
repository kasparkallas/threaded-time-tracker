﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;

namespace Api.Models
{
    public class TimeThreadDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool IsBillable { get; set; }
        public Guid UserId { get; set; }
        public Guid? ParentId { get; set; }
        public List<TimeThreadDto> Children { get; set; } = new List<TimeThreadDto>();
        public DateTimeOffset StartedAt { get; set; }
        public DateTimeOffset? StoppedAt { get; set; }
        public int LevelsDeep { get; set; }

        public TimeThreadDto()
        {
        }

        public TimeThreadDto(TimeThread thread)
        {
            Id = thread.Id;
            Description = thread.Description;
            IsBillable = thread.IsBillable;
            UserId = thread.UserId;
            ParentId = thread.ParentId;
            StartedAt = thread.StartedAt;
            StoppedAt = thread.StoppedAt;
            LevelsDeep = thread.LevelsDeep;
            Children.AddRange(thread.Children.Select(childThread => new TimeThreadDto(childThread)));
        }
    }
}