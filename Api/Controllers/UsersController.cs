﻿using System;
using System.Linq;
using System.Web.Http;
using Domain;

namespace Api.Controllers
{
    [Authorize]
    [Route("api/users")]
    public class UsersApiController : ApiController
    {
        private readonly IRepository _repository;

        public UsersApiController(IRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_repository.Query<User>().ToList());
        }

        [HttpPost]
        [Route("api/users")]
        public IHttpActionResult Post([FromBody]Guid userId)
        {
            if (userId == default(Guid))
            {
                return BadRequest($"{nameof(userId)}: {userId}");
            }

            var user = new User(userId);

            _repository.Add(user);

            return Created(Request.RequestUri.ToString() + userId, user);
        }
    }
}
