﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Api.Models;
using Domain;
using Microsoft.AspNet.Identity;

namespace Api.Controllers
{
    [Authorize]
    [Route("api/threads")]
    public class ThreadsController : ApiController
    {
        private readonly IRepository _repository;
        private readonly TimeTrackerService _timeTracker;

        public ThreadsController(IRepository repository, TimeTrackerService timeTracker)
        {
            _repository = repository;
            _timeTracker = timeTracker;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            var allThreads = _repository.Query<TimeThread>()
                .ToList();
                
            return Ok(allThreads
                .Select(x => new TimeThreadDto(x)));
        }

        [HttpGet]
        [Route("api/threads/user/{userId}")]
        public IHttpActionResult GetByUser(Guid userId)
        {
            if (userId == default(Guid))
            {
                return BadRequest($"{nameof(userId)}: {userId}");
            }

            var currentUserId = new Guid(this.User.Identity.GetUserId());
            if (currentUserId != userId)
            {
                return Unauthorized();
            }

            var userThreads = _repository.Query<TimeThread>()
                .Include(x => x.Children)
                .Where(x => x.UserId == userId)
                .ToList();

            return Ok(userThreads
                .Select(x => new TimeThreadDto(x)));
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult Get(Guid id)
        {
            if (id == default(Guid))
            {
                return BadRequest($"{nameof(id)}: {id}");
            }

            var thread = _repository.Load<TimeThread>(id);
            if (thread == null)
            {
                return NotFound();
            }

            return Ok(new TimeThreadDto(thread));
        }

        [HttpPost]
        [Route("api/threads/start")]
        public IHttpActionResult Start([FromBody]StartTrackingTimeBindingModel model)
        {
            var user = _repository.Load<User>(model.UserId);
            if (user == null)
            {
                return BadRequest($"User not found with id: {model.UserId}");
            }

            var currentUserId = new Guid(this.User.Identity.GetUserId());
            if (currentUserId != user.Id)
            {
                return Unauthorized();
            }

            TimeThread newThread;
            if (model.ParentThreadId.HasValue)
            {
                var parentThread = _repository.Load<TimeThread>(model.ParentThreadId.Value);

                if (parentThread == null)
                {
                    return BadRequest($"Parent thread not found with id: {model.ParentThreadId}");
                }

                newThread = _timeTracker.StartThread(user, parentThread, model.Description);
            }
            else
            {
                newThread = _timeTracker.StartThread(user, model.Description);
            }

            var newThreadDto = new TimeThreadDto(newThread);
            return Created(Request.RequestUri.ToString() + newThread.Id, newThreadDto);
        }

        [HttpPost]
        [Route("api/threads/stop")]
        public IHttpActionResult Stop([FromBody]Guid threadId)
        {
            var thread = _repository.Load<TimeThread>(threadId);
            if (thread == null)
            {
                return BadRequest($"Time thread not found with id: {threadId}");
            }

            var currentUserId = new Guid(this.User.Identity.GetUserId());
            if (currentUserId != thread.UserId)
            {
                return Unauthorized();
            }

            _timeTracker.StopThread(thread);

            return Ok();
        }

    }
}
