﻿using System;
using System.Linq;
using Domain;
using Raven.Client;

namespace Api
{
    public class RavenRepository : IRepository
    {
        private readonly IDocumentSession _documentSession;

        public RavenRepository(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        public void Add<T>(T entity) where T : class
        {
            _documentSession.Store(entity);
            _documentSession.SaveChanges();
        }

        public void Update<T>(T entity) where T : class
        {
            _documentSession.Store(entity);
            _documentSession.SaveChanges();
        }

        public T Load<T>(Guid id) where T : class
        {
            return _documentSession.Load<T>(id);
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return _documentSession.Query<T>();
        }
    }
}