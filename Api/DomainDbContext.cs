﻿using Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Api
{
    public class DomainDbContext : DbContext
    {
        public IDbSet<TimeThread> TimeThreads { get; set; }
        public IDbSet<User> Users { get; set; }

        public DomainDbContext() : base("DefaultConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DomainDbContext>());

            base.OnModelCreating(modelBuilder);
        }
    }
}